package com.example.test_4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.mopub.mobileads.MoPubView;
import com.mopub.common.SdkConfiguration;

public class MainActivity extends AppCompatActivity {

    @Override
    private MoPubView moPubView;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        moPubView = (MoPubView) findViewById(R.id.adview);
        moPubView.setAdUnitId("f0eb6a4844c14b5baad41a0ba4b34b83"); // Enter your Ad Unit ID from www.mopub.com
        moPubView.loadAd();
    }



}
